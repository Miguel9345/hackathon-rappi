package com.rappi.hackathon.vo;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="orders")
public class Orders {
	@Id
	public ObjectId _id;
	private Integer id;
	private Double lat;
	private Double lng;
	private String timestamp;
	private String createdAt;
	private String type;
	private Toolkit toolkit;

	public Integer getId() {
		
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Toolkit getToolkit() {
		return toolkit;
	}

	public void setToolkit(Toolkit toolkit) {
		this.toolkit = toolkit;
	}
	@Override
	public String toString() {
		return "Orders [id=" + id + ", lat=" + lat + ", lng=" + lng + ", timestamp=" + timestamp + ", createdAt="
				+ createdAt + ", type=" + type + ", toolkit=" + toolkit + "]";
	}
	
	
}