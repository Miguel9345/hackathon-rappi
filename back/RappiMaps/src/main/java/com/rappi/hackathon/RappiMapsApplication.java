package com.rappi.hackathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@SpringBootApplication
@EnableDiscoveryClient
public class RappiMapsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RappiMapsApplication.class, args);
	}
}
