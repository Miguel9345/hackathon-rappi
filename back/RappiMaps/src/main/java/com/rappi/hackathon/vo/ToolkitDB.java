package com.rappi.hackathon.vo;

public class ToolkitDB {

	private Boolean trusted;
	private Integer vehicle;
	private Integer kitSize;
	private Integer knowHow;
	private Integer terminal;
	private Boolean exclusive;
	private Integer orderLevel;
	private Integer deliveryKit;
	private Integer storekeeperLevel;

	public Boolean getTrusted() {
		return trusted;
	}

	public void setTrusted(Boolean trusted) {
		this.trusted = trusted;
	}

	public Integer getVehicle() {
		return vehicle;
	}

	public void setVehicle(Integer vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getKitSize() {
		return kitSize;
	}

	public void setKitSize(Integer kitSize) {
		this.kitSize = kitSize;
	}

	public Integer getKnowHow() {
		return knowHow;
	}

	public void setKnowHow(Integer knowHow) {
		this.knowHow = knowHow;
	}

	public Integer getTerminal() {
		return terminal;
	}

	public void setTerminal(Integer terminal) {
		this.terminal = terminal;
	}

	public Boolean getExclusive() {
		return exclusive;
	}

	public void setExclusive(Boolean exclusive) {
		this.exclusive = exclusive;
	}

	public Integer getOrderLevel() {
		return orderLevel;
	}

	public void setOrderLevel(Integer orderLevel) {
		this.orderLevel = orderLevel;
	}

	public Integer getDeliveryKit() {
		return deliveryKit;
	}

	public void setDeliveryKit(Integer deliveryKit) {
		this.deliveryKit = deliveryKit;
	}

	public Integer getStorekeeperLevel() {
		return storekeeperLevel;
	}

	public void setStorekeeperLevel(Integer storekeeperLevel) {
		this.storekeeperLevel = storekeeperLevel;
	}

}