package com.rappi.hackathon.bridge;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.rappi.hackathon.exception.InvalidDataException;
import com.rappi.hackathon.exception.RappiMapsException;
import com.rappi.hackathon.model.OrdersRepository;
import com.rappi.hackathon.model.StorekeepersRepository;
import com.rappi.hackathon.util.Util;
import com.rappi.hackathon.vo.Orders;
import com.rappi.hackathon.vo.StorekeeperDB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class QueryServiceImpl implements QueryService {
	private static final Logger log = LoggerFactory.getLogger(QueryServiceImpl.class);

	@Autowired
	private StorekeepersRepository storekeepersRepository;

	@Autowired
	private OrdersRepository ordersRepository;

	@Override
	public ArrayList<StorekeeperDB> searchStorekeepers(String... data) throws RappiMapsException {
		if (Util.StringUtils.isValid(data)) {
			String initialDate = Util.DateUtils.formatDate(data[0]);
			String finalDate = Util.DateUtils.formatDate(data[1]);
			log.info(initialDate);
			log.info(finalDate);
			ArrayList<String> _out = storekeepersRepository.findStorekeepersBetweenDate(initialDate, finalDate);
			ArrayList<com.rappi.hackathon.vo.StorekeeperDB> objectResult = new ArrayList<>();
			Gson gson = new Gson();
			for (String item : _out) {
				com.rappi.hackathon.vo.StorekeeperDB _jsonOut = gson.fromJson(item,
						com.rappi.hackathon.vo.StorekeeperDB.class);
				objectResult.add(_jsonOut);
			}
			return objectResult;
		} else {
			throw new InvalidDataException("Invalid params");
		}
	}

	@Override
	public ArrayList<Orders> searchOrders(String... data) throws RappiMapsException {
		if (Util.StringUtils.isValid(data)) {
			String initialDate = Util.DateUtils.formatDate(data[0]);
			String finalDate = Util.DateUtils.formatDate(data[1]);
			log.info(initialDate);
			log.info(finalDate);
			if (Util.StringUtils.isValid(data[2]) && Util.StringUtils.isValid(data[3])) {
				String val = data[2];
				if ("type".equalsIgnoreCase(data[3])) {
					return ordersRepository.findByTypeAndDate(initialDate,finalDate,val);
				}else if("vehicle".equalsIgnoreCase(data[4])) {
					return ordersRepository.findByVehicleAndDate(initialDate, finalDate, val); 
				}else {
					return ordersRepository.findAllByDate(initialDate, finalDate); 
				}
			}else {
				return ordersRepository.findAllByDate(initialDate, finalDate);
			}			
		} else {
			throw new InvalidDataException("Invalid params");
		}
	}

}
