package com.rappi.hackathon.exception;

public class InvalidDatesException extends InvalidDataException{

	private static final long serialVersionUID = 1L;

	public InvalidDatesException() {
		super();
	}

	public InvalidDatesException(String message, Throwable cause, boolean enableSuppression,boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidDatesException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDatesException(String message) {
		super(message);		
	}

	public InvalidDatesException(Throwable cause) {
		super(cause);
	}
	
	
	

}
