package com.rappi.hackathon.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rappi.hackathon.bridge.QueryService;
import com.rappi.hackathon.exception.RappiMapsException;
import com.rappi.hackathon.vo.Orders;
import com.rappi.hackathon.vo.StorekeeperDB;


@Controller
public class ServiceController {
	@SuppressWarnings("unused")
	@Autowired
	private Environment environment;
	
	@Autowired
	private  QueryService queryService;	
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ServiceController.class);

	@CrossOrigin
	@GetMapping("api/v1/query/storekeppers")
	@ResponseBody
	public ArrayList<StorekeeperDB> queryStoreKeppers(
			@RequestParam(value="id", defaultValue="") String initialDate,
			@RequestParam(value="fd", defaultValue="") String finalDate,
			@RequestParam(value="ski", defaultValue="") String storekeeperId
			) {
		try {
			return queryService.searchStorekeepers(initialDate,finalDate,storekeeperId);

		} catch (RappiMapsException e) {
			return null;
		}
	}
	@CrossOrigin
	@GetMapping("api/v1/query/orders")
	@ResponseBody
	public ArrayList<Orders> test(
			@RequestParam(value="id", defaultValue="") String initialDate,
			@RequestParam(value="fd", defaultValue="") String finalDate,
			@RequestParam(value="val", defaultValue="") String value,
			@RequestParam(value="tp", defaultValue="") String type
			) {
		try {
			 return queryService.searchOrders(initialDate,finalDate,value,type);
			
		} catch (RappiMapsException e) {
			return null;
		}
	}

}
