package com.rappi.hackathon.model;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rappi.hackathon.entity.StorekeepersObjectDB;

public interface StorekeepersRepository extends CrudRepository<StorekeepersObjectDB, Integer> {
	@Query(nativeQuery=true,name="StorekeepersRepository.findStorekeepersBetweenDate",value=""+
			"  select ''||row_to_json(rappiTenderos) " + 
			"  from (select * " + 
			"  from storekeepers " + 
			"  where(timestamp between " + 			
			"			cast(?1 as timestamp) " +
			"			and  " + 			
			"			cast(?2 as timestamp)  + interval '30 minutes')"+
			"   and toolkit->>'trusted' in ('false', 'true') " + 
			"	and toolkit->>'vehicle' in ('2','1','0') " + 
			"	and toolkit->>'kit_size' in ('1','11','12','2','3') " + 
			"	and toolkit->>'know_how' in ('0','1') " + 
			"	and toolkit->>'terminal' in ('0','1') " + 
			"	and toolkit->>'exclusive' in ('true','false') " + 
			"	and toolkit->>'order_level' in ('1','2','3','4','5','6','7','8','9') " + 
			"	and toolkit->>'delivery_kit' in ('0','1') " + 
			"	and toolkit->>'storekeeper_level' in ('2')) as rappiTenderos")
	ArrayList<String> findStorekeepersBetweenDate(String initialDate,String finalDate);
}
