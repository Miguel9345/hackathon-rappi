package com.rappi.hackathon.vo;

public class Toolkit {
	private Integer deliveryKit;
	private Integer kitSize;
	private Integer terminal;
	private Integer knowHow;
	private Boolean trusted;
	private Integer orderLevel;
	private Integer storekeeperLevel;
	private Integer vehicle;
	private Boolean cashless;
	private Boolean exclusive;
	public Integer getDeliveryKit() {
	return deliveryKit;
	}

	public void setDeliveryKit(Integer deliveryKit) {
	this.deliveryKit = deliveryKit;
	}

	public Integer getKitSize() {
	return kitSize;
	}

	public void setKitSize(Integer kitSize) {
	this.kitSize = kitSize;
	}

	public Integer getTerminal() {
	return terminal;
	}

	public void setTerminal(Integer terminal) {
	this.terminal = terminal;
	}

	public Integer getKnowHow() {
	return knowHow;
	}

	public void setKnowHow(Integer knowHow) {
	this.knowHow = knowHow;
	}

	public Boolean getTrusted() {
	return trusted;
	}

	public void setTrusted(Boolean trusted) {
	this.trusted = trusted;
	}

	public Integer getOrderLevel() {
	return orderLevel;
	}

	public void setOrderLevel(Integer orderLevel) {
	this.orderLevel = orderLevel;
	}

	public Integer getStorekeeperLevel() {
	return storekeeperLevel;
	}

	public void setStorekeeperLevel(Integer storekeeperLevel) {
	this.storekeeperLevel = storekeeperLevel;
	}

	public Integer getVehicle() {
	return vehicle;
	}

	public void setVehicle(Integer vehicle) {
	this.vehicle = vehicle;
	}

	public Boolean getCashless() {
	return cashless;
	}

	public void setCashless(Boolean cashless) {
	this.cashless = cashless;
	}

	public Boolean getExclusive() {
	return exclusive;
	}

	public void setExclusive(Boolean exclusive) {
	this.exclusive = exclusive;
	}
}
