package com.rappi.hackathon.model;

import java.util.ArrayList;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.rappi.hackathon.vo.Orders;

public interface OrdersRepository extends MongoRepository<Orders, Integer>{
	@Query("{type:'?0'}")
	ArrayList<Orders> findByType(String type);
	
	@Query("{$or:[{timestamp:'?0'},{timestamp:'?1'}]}")
	ArrayList<Orders> findAllByDate(String initialDate,String finalDate);
	
	@Query("{$or:[{timestamp:'?0'},{timestamp:'?1'}],$and:[{type:'?2'}]}")
	ArrayList<Orders> findByTypeAndDate(String initialDate,String finalDate,String type);
	
	@Query("{$or:[{timestamp:'?0'},{timestamp:'?1'}],$and:[{toolkit.vehicle: {$in:[?2]}}]}")
	ArrayList<Orders> findByVehicleAndDate(String initialDate,String finalDate,String vehicle);
}
