package com.rappi.hackathon.vo;

public class StorekeeperDB {

	private Integer id;
	private Integer storekeeperId;
	private Double lat;
	private Double lng;
	private String timestamp;
	private ToolkitDB toolkit;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStorekeeperId() {
		return storekeeperId;
	}

	public void setStorekeeperId(Integer storekeeperId) {
		this.storekeeperId = storekeeperId;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public ToolkitDB getToolkit() {
		return toolkit;
	}

	public void setToolkit(ToolkitDB toolkit) {
		this.toolkit = toolkit;
	}

	@Override
	public String toString() {
		return "Storekeeper [id=" + id + ", storekeeperId=" + storekeeperId + ", lat=" + lat + ", lng=" + lng
				+ ", timestamp=" + timestamp + ", toolkit=" + toolkit + "]";
	}

}