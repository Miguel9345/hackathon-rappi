package com.rappi.hackathon.bridge;

import java.util.ArrayList;


import com.rappi.hackathon.exception.RappiMapsException;
import com.rappi.hackathon.vo.Orders;
import com.rappi.hackathon.vo.StorekeeperDB;

public interface QueryService {
	public ArrayList<Orders> searchOrders(String ... data) throws RappiMapsException;
	public ArrayList<StorekeeperDB> searchStorekeepers(String ... data) throws RappiMapsException;
}
