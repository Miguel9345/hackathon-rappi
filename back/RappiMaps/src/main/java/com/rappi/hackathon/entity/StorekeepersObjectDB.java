package com.rappi.hackathon.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Walter.Forero
 */
@Entity
@Table(name = "storekeepers")
@Cacheable(false)
@XmlRootElement
public class StorekeepersObjectDB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "storekeeper_id")
    private Integer storekeeperId;
    @Column(name = "lat")
    private BigDecimal lat;
    @Column(name = "lng")
    private BigDecimal lng;
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    @Lob
    @Column(name = "toolkit")
    private Object toolkit;

    public StorekeepersObjectDB() {
    }

    public StorekeepersObjectDB(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStorekeeperId() {
        return storekeeperId;
    }

    public void setStorekeeperId(Integer storekeeperId) {
        this.storekeeperId = storekeeperId;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Object getToolkit() {
        return toolkit;
    }

    public void setToolkit(Object toolkit) {
        this.toolkit = toolkit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof StorekeepersObjectDB)) {
            return false;
        }
        StorekeepersObjectDB other = (StorekeepersObjectDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Storekeepers [id=" + id + ", storekeeperId=" + storekeeperId + ", lat=" + lat + ", lng=" + lng + ", timestamp=" + timestamp + ", toolkit=" + toolkit + "]";
	}


    
}

