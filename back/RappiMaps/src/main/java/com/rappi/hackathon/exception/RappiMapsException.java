package com.rappi.hackathon.exception;

public class RappiMapsException extends Exception{

	private static final long serialVersionUID = -2217472724809837329L;

	public RappiMapsException() {
		super();
	}

	public RappiMapsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RappiMapsException(String message, Throwable cause) {
		super(message, cause);
	}

	public RappiMapsException(String message) {
		super(message);
	}

	public RappiMapsException(Throwable cause) {
		super(cause);
	}
	
	

}
