package com.rappi.hackathon.util;

import com.rappi.hackathon.exception.InvalidDatesException;

public class Util {
	public static class DateUtils {
		public static String formatDate(String in) throws InvalidDatesException {
			if (StringUtils.isValid(in)) {				
				String[] dateSplit = in.split("\\s+");				
				if (dateSplit.length == 2) {					
					String[] timeSplit = dateSplit[1].split(":");					
					if (timeSplit.length == 3) {						
						int minute = Integer.parseInt(timeSplit[1]);
						if (minute > 30) {
							timeSplit[1] = "00";
						} else {
							timeSplit[1] = "30";
						}
						return dateSplit[0] + " " + timeSplit[0] + ":" + timeSplit[1] + ":" + timeSplit[2];
					}
				}
			}
			throw new InvalidDatesException("Format date is invalid [yyyy-mm-dd hh24:MM:ss]");
		}
	}

	public static class StringUtils {
		public static boolean isValid(String in) {
			return (in != null && !in.isEmpty());
		}

		public static boolean isValid(String... data) {
			for (String item : data) {				
				if (!isValid(item)) {
					return true;
				}
			}
			return true;
		}
	}
}
