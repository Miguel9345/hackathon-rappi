﻿SELECT distinct(toolkit->>'trusted' )
FROM storekeepers;

SELECT distinct(toolkit->>'vehicle' )
FROM storekeepers;

SELECT distinct(toolkit->>'kit_size' )
FROM storekeepers;

SELECT distinct(toolkit->>'know_how' )
FROM storekeepers
order by 1;

SELECT distinct(toolkit->>'terminal' )
FROM storekeepers
order by 1;

SELECT distinct(toolkit->>'exclusive' )
FROM storekeepers
order by 1;

SELECT distinct(toolkit->>'order_level' )
FROM storekeepers
order by 1;

SELECT distinct(toolkit->>'delivery_kit' )
FROM storekeepers
order by 1;

SELECT distinct(toolkit->>'storekeeper_level' )
FROM storekeepers
order by 1;