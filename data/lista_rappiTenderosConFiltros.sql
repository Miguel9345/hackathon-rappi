﻿--retorna tbala
select *
from storekeepers
where (timestamp between 
		(to_char('2018-09-07 20:19:00'::timestamp,'yyyy-mm-dd HH24')||':'||case when to_char('2018-09-07 20:19:00'::timestamp,'mm')::int > 30 then '30' else '00' end||':00')::timestamp
		and  
		(to_char('2018-09-07 20:19:00'::timestamp,'yyyy-mm-dd HH24')||':'||case when to_char('2018-09-07 20:19:00'::timestamp,'mm')::int > 30 then '30' else '00' end||':00')::timestamp  + interval '30 minutes')
	and toolkit->>'trusted' in ('false', 'true')
	and toolkit->>'vehicle' in ('2','1','0')
	and toolkit->>'kit_size' in ('1','11','12','2','3')
	and toolkit->>'know_how' in ('0','1')
	and toolkit->>'terminal' in ('0','1')
	and toolkit->>'exclusive' in ('true','false')
	and toolkit->>'order_level' in ('1','2','3','4','5','6','7','8','9')
	and toolkit->>'delivery_kit' in ('0','1')
	and toolkit->>'storekeeper_level' in ('2')


--Retorna json

select row_to_json(rappiTenderos) 
from (select *
from storekeepers
where (timestamp between 
		(to_char('2018-09-07 20:19:00'::timestamp,'yyyy-mm-dd HH24')||':'||case when to_char('2018-09-07 20:19:00'::timestamp,'mm')::int > 30 then '30' else '00' end||':00')::timestamp
		and  
		(to_char('2018-09-07 20:19:00'::timestamp,'yyyy-mm-dd HH24')||':'||case when to_char('2018-09-07 20:19:00'::timestamp,'mm')::int > 30 then '30' else '00' end||':00')::timestamp  + interval '30 minutes')
	and toolkit->>'trusted' in ('false', 'true')
	and toolkit->>'vehicle' in ('2','1','0')
	and toolkit->>'kit_size' in ('1','11','12','2','3')
	and toolkit->>'know_how' in ('0','1')
	and toolkit->>'terminal' in ('0','1')
	and toolkit->>'exclusive' in ('true','false')
	and toolkit->>'order_level' in ('1','2','3','4','5','6','7','8','9')
	and toolkit->>'delivery_kit' in ('0','1')
	and toolkit->>'storekeeper_level' in ('2')) as rappiTenderos;