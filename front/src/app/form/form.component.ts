import {Component, OnInit} from '@angular/core';
import {ResourcesService} from '../services/resources.service';
import { FilterModel } from './filterModel';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [ResourcesService]
})

export class FormComponent implements OnInit {
  public lat: number;
  public lng: number;
  public loading = false;
  public map_locations: Array<object>;
  public response: string;
  public filterData = {
    geoHash: '',
    initialDate: '',
    initialHour: '',
    finalDate: '',
    finalHour: '',
    vehicleType: '',
    orderType: ''
  };

  constructor(
    private generalResourceService: ResourcesService
  ) {}
  ngOnInit() {
    this.lat = 4.7659407306833;
    this.lng = -74.0396103868814;
    this.map_locations = [];
    this.generalResourceService.currentData.subscribe(data => this.response = data);
  }

  sendData() {
    this.map_locations = [];
    let filterDataToSend = {
      id: this.filterData.initialDate + ' ' + this.filterData.initialHour + ':00',
      fd: this.filterData.finalDate + ' ' + this.filterData.finalHour + ':00',
    };
    this.generalResourceService.getData(filterDataToSend).subscribe(
      result => {
        let markers = result;
        this.map_locations.push(markers);
      },
      error => {
         console.log(error);
      }
    );
  }
}
