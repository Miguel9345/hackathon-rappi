import {Component, OnInit} from '@angular/core';
import {ResourcesService} from '../services/resources.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [ResourcesService]
})


export class MapComponent implements OnInit {
  lat: number;
  lng: number;
  public map_locations: Array<object>;
  public response: string = '{"":0}';
  constructor(
    private generalResourceService: ResourcesService
  ) {
  }
  ngOnInit() {
    this.lat = 4.7659407306833;
    this.lng = -74.0396103868814;
    /*this.map_locations = [
      {latitude: 4.7659407306833, longitude: -74.0396103868814},
      {latitude: 4.72911898828937, longitude: -74.0236786054174},
      {latitude: 4.73347334027383, longitude: -74.0184858768495},
    ];*/
    this.generalResourceService.currentData.subscribe(data => this.response = data);
  }
}
