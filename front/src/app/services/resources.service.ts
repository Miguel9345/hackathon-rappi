import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {BehaviorSubject} from 'rxjs';

@Injectable()

export class ResourcesService {
  protected url: string = 'http://jarcidco.com:8080/RappiMaps/api/v1/query/storekeepers';
  private headers: HttpHeaders;

  private response = new BehaviorSubject('');
  currentData = this.response.asObservable();

  constructor(
    private httpClient: HttpClient,
  ) {
    this.headers = new HttpHeaders({'Content-type': 'application/json'});
  }

  public getData(token): Observable<any> {
    let httpParams = new HttpParams();
    Object.keys(token).forEach(function (key) {
      httpParams = httpParams.append(key, token[key]);
    });
    return this.httpClient.get<any>( this.url, {headers: this.headers, params: httpParams});
  }

  public setData(response: string) {
    this.response.next(response);
  }
}
